# My portfolio
Hi, I would like to present some reports made for uTEST platform:  
   
**Reebok**

- [Bug report](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/Reebok/Reebok.docx)
- [Bug screen](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/Reebok/reebok.jpg)
- [Bug record](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/Reebok/reebok.mp4)

**Puma**

- [Bug report](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/Puma/Puma.docx)
- [Bug screen](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/Puma/screen.jpg)
- [Bug record](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/Puma/Recording.mp4)

**UnderArmour**

- [Bug report](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/UnderArmour/UnderArmour.docx)
- [Bug screen](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/UnderArmour/bug.jpg)

**JOANN - mobile app**
  
1. 
- [Bug report](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/JOANN/1/JOANN_cart.docx)
- [Bug screen](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/JOANN/1/Screenshot_2021-05-13-15-55-03-960_com.fifthfinger.clients.joann.jpg)
- [Bug record](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/JOANN/1/Screenrecorder-2021-05-13-15-55-37-83.mp4)

2. 
- [Bug report](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/JOANN/2/JOANN_home.docx)
- [Bug screen](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/JOANN/2/Screenshot_2021-05-13-15-57-26-056_com.fifthfinger.clients.joann.jpg)
- [Bug record](https://gitlab.com/tomaszko1989/my-portfolio/-/blob/main/Bugs%20from%20uTest/JOANN/2/Screenrecorder-2021-05-13-15-56-55-826.mp4)
